package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import static org.junit.Assert.*;

public class SeveralDeclarationErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new SeveralDeclarationError(null,"variableName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullVariableName() {
        new SeveralDeclarationError(new CaretPosition(0,0),null);
    }

}