package errors;

import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNSimulator;
import org.junit.Test;

import static org.junit.Assert.*;

public class ErrorListenerTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCompilationErrorList() {
        new ErrorListener(null);
    }

}