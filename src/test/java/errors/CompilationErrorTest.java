package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompilationErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new CompilationError(null, "error");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullMessage() {
        new CompilationError(new CaretPosition(), null);
    }

    @Test
    public void messageShouldNotBeEmpty() {
        CompilationError compilationError = new CompilationError(new CaretPosition(0, 0), "error");
        assertFalse(compilationError.getMessage().length()==0);
    }

}