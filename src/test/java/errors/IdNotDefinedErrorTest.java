package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import static org.junit.Assert.*;

public class IdNotDefinedErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new IdNotDefinedError(null, "variable");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullString() {
        new IdNotDefinedError(new CaretPosition(0, 0), null);
    }

}