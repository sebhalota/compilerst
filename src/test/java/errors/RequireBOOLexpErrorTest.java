package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

public class RequireBOOLexpErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new RequireBOOLexpError(null,"name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullName() {
        new RequireBOOLexpError(new CaretPosition(0,0),null);
    }

}