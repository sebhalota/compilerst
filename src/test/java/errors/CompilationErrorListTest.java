package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CompilationErrorListTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastNullPointerException() {
        CompilationErrorList list = new CompilationErrorList();
        list.add(null);
    }

    @Test
    public void shouldReturnTrue() {
        CompilationErrorList list = new CompilationErrorList();
        assertTrue(list.noErrors());
    }

    @Test
    public void shouldReturnFalse() {
        CompilationErrorList list = new CompilationErrorList();
        list.add(new CompilationError(new CaretPosition(0,0),"error"));
        assertFalse(list.noErrors());
    }

    @Test
    public void shouldReturnEmptyList() {
        CompilationErrorList list = new CompilationErrorList();
        assertTrue(list.getErrorsMessages().size() == 0);
    }

    @Test
    public void shouldReturnNonEmptyList() {
        CompilationErrorList list = new CompilationErrorList();
        list.add(new CompilationError(new CaretPosition(0,0),"error"));
        assertTrue(list.getErrorsMessages().size() != 0);
    }

}