package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import static org.junit.Assert.*;

public class TypeMismatchErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new IdNotDefinedError(null, "varableName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullVariableName() {
        new IdNotDefinedError(new CaretPosition(0, 0), null);
    }

}