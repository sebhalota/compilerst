package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

public class TypeMismatchInParaErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new TypeMismatchInParaError(null, 0, "paraSign", "type");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullParaSign() {
        new TypeMismatchInParaError(new CaretPosition(0, 0), 0, null, "type");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullType() {
        new TypeMismatchInParaError(new CaretPosition(0, 0), 0, "paraSign", null);
    }


}