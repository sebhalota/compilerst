package errors;

import CaretPosition.CaretPosition;
import org.junit.Test;

import static org.junit.Assert.*;

public class ErroneousInitialValueErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullCaretPosition() {
        new ErroneousInitialValueError(null,"variable");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentExceptionWithNullVariableName() {
        new ErroneousInitialValueError(new CaretPosition(0,0),null);
    }


}