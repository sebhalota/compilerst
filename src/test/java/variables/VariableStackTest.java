package variables;

import CaretPosition.CaretPosition;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.Assert.*;


public class VariableStackTest {

    private VariableStack variableStack = new VariableStack();

    @Test
    public void shouldPopNull() {
        assertNull(variableStack.pop());
    }

    @Test
    public void shouldNotPopNull() {
        variableStack.push(new SymbolBOOL("test",new CaretPosition(1,1)));
        assertNotNull(variableStack.pop());
    }

    @Test
    public void shouldSayThatSizeIsThree() {
        assertTrue(variableStack.isSizeTry(new int[3]));
    }

    @Test
    public void shouldSayThatSizeIsNotThree() {
        assertFalse(variableStack.isSizeTry(null));
        assertFalse(variableStack.isSizeTry(new int[5]));
    }

    @ParameterizedTest
    @ValueSource(ints = {0,2,4,6,8})
    public void shouldSayThatThoseNumbersAreEven(int p) {
        assertTrue(variableStack.even(p));
    }





}