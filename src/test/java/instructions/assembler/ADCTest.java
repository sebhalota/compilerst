package instructions.assembler;

import org.junit.Test;

import static org.junit.Assert.*;

public class ADCTest {

    @Test
    public void shouldReturnAssemblerCode_Case1() {
        ADC adc = new ADC((byte)16,(byte)16);
        assertEquals(adc.getAssemblerCodeImpl(),"adc r16,r16");
    }

    @Test
    public void shouldReturnAssemblerCode_Case2() {
        ADC adc = new ADC((byte)16,(byte)31);
        assertEquals(adc.getAssemblerCodeImpl(),"adc r16,r31");
    }

    @Test
    public void shouldReturnAssemblerCode_Case3() {
        ADC adc = new ADC((byte)23,(byte)27);
        assertEquals(adc.getAssemblerCodeImpl(),"adc r23,r27");
    }

}