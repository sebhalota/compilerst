package instructions.assembler;

import org.junit.Test;

import static org.junit.Assert.*;

public class ADDTest {

    @Test
    public void shouldReturnAssemblerCode_Case1() {
        ADD add = new ADD((byte)16,(byte)16);
        assertEquals(add.getAssemblerCodeImpl(),"add r16,r16");
    }

    @Test
    public void shouldReturnAssemblerCode_Case2() {
        ADD add = new ADD((byte)16,(byte)31);
        assertEquals(add.getAssemblerCodeImpl(),"add r16,r31");
    }

    @Test
    public void shouldReturnAssemblerCode_Case3() {
        ADD add = new ADD((byte)23,(byte)27);
        assertEquals(add.getAssemblerCodeImpl(),"add r23,r27");
    }


}