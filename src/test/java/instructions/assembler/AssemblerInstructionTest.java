package instructions.assembler;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class AssemblerInstructionTest {

    private static AssemblerInstruction assemblerInstructionPattern16;
    private static AssemblerInstruction assemblerInstructionPattern32_1;
    private static AssemblerInstruction assemblerInstructionPattern32_2;

    @BeforeClass
    public static void setUp() {
        assemblerInstructionPattern16 = new AssemblerInstruction("0000000000000000", 0, 0) {
            @Override
            protected String getName() {
                return null;
            }

            @Override
            protected String getAssemblerCodeImpl() {
                return null;
            }
        };
        assemblerInstructionPattern32_1 = new AssemblerInstruction("00000000000000000000000000000000", 0, 0) {
            @Override
            protected String getName() {
                return null;
            }

            @Override
            protected String getAssemblerCodeImpl() {
                return null;
            }
        };
        assemblerInstructionPattern32_2 = new AssemblerInstruction("00000000000000000000000000000000", 0, 0) {
            @Override
            protected String getName() {
                return null;
            }

            @Override
            protected String getAssemblerCodeImpl() {
                return null;
            }
        };

    }

    @Test
    public void shouldReturnSizeInBytesEquals2() {
        assertTrue(assemblerInstructionPattern16.getSizeInBytes() == 2);
    }

    @Test
    public void shouldReturnSizeInBytesEquals4() {
        assertTrue(assemblerInstructionPattern32_1.getSizeInBytes() == 4);
    }

//    @Test
//    public void shouldReturnAddressEquals0() {
//        assertTrue(assemblerInstructionPattern16.getAddress() == 0);
//    }
//
//    @Test
//    public void shouldReturnAddressEquals1() {
//        assertTrue(assemblerInstructionPattern32_1.getAddress() == 1);
//    }
//
//    @Test
//    public void shouldReturnAddressEquals3() {
//        assertTrue(assemblerInstructionPattern32_2.getAddress() == 3);
//    }


}