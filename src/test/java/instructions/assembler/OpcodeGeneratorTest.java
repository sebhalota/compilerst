package instructions.assembler;

import org.junit.Test;

import static org.junit.Assert.*;

public class OpcodeGeneratorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentException_NullPattern() {
        new OpcodeGenerator(null, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentException_InvalidPatternLength() {
        new OpcodeGenerator("00", 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCastIllegalArgumentException_InvalidCharacters() {
        new OpcodeGenerator("000000X000000000", 0, 0);
    }

    @Test
    public void shouldNotCastIllegalArgumentException_LengthIs16() {
        new OpcodeGenerator("1010AB10AB10AB10", 0, 0);
    }

    @Test
    public void shouldNotCastIllegalArgumentException_LengthIs32() {
        new OpcodeGenerator("1010AB10AB10AB101010AB10AB10AB10", 0, 0);
    }

    @Test
    public void shouldReturn101010111111111() {
        OpcodeGenerator opcodeGenerator = new OpcodeGenerator("1111111101010101", 0, 0);
        int expectedOpcode = 0b101010111111111;
        assertTrue(expectedOpcode == opcodeGenerator.getOpcode());
    }

    @Test
    public void shouldReturn10101011() {
        OpcodeGenerator opcodeGenerator = new OpcodeGenerator("10AB10AB00000000", 0xF, 1);
        int expectedOpcode = 0b10101011;
        assertTrue(expectedOpcode == opcodeGenerator.getOpcode());
    }

    @Test
    public void shouldReturn10101001101010011010100110101001() {
        OpcodeGenerator opcodeGenerator = new OpcodeGenerator("10AB10AB10AB10AB10AB10AB10AB10AB", 0b10101010, 0b01010101);
        int expectedOpcode = 0b10101001101010011010100110101001;
        assertTrue(expectedOpcode == opcodeGenerator.getOpcode());
    }


}