package instructions.assembler;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.Assert.*;

public class RegisterRTest {

    @ParameterizedTest
    @ValueSource(ints = {-42, 0, 15, 6, 32, 255})
    public void shouldCastIllegalArgumentException(int b) {

        boolean thrown = false;

        try {
            new RegisterR((byte) b);
        } catch (IllegalArgumentException e) {
            thrown = true;
        }

        assertTrue(thrown);
    }

    @ParameterizedTest
    @ValueSource(ints = {16, 20, 31})
    public void shouldntCastIllegalArgumentException(int b) {

        boolean thrown = false;

        try {
            new RegisterR((byte) b);
        } catch (IllegalArgumentException e) {
            thrown = true;
        }

        assertFalse(thrown);
    }

}