grammar StGrammar;
import StTokens, Expression;

program: PRG VAR declarations endOfDeclaration commands endOfProgram
;

declarations: /* empty */
   | declarations declaration SCOL
;

endOfDeclaration: END_VAR;
endOfProgram    : END_PROGRAM;

declaration: ID (
    declarationBOOL
    | declarationOUTPUT
    | declarationINPUT
    | declarationBYTE
    | declarationWORD
    )
;

declarationBOOL: COL BOOL (|(ASSIGN (FALSE | TRUE)));
declarationOUTPUT: AT OUTPUT NUMBER DOT NUMBER COL BOOL (|(ASSIGN (FALSE | TRUE)));
declarationINPUT: AT INPUT NUMBER DOT NUMBER COL BOOL (|(ASSIGN (FALSE | TRUE)));
declarationBYTE: COL BYTE (|(ASSIGN NUMBER));
declarationWORD: COL WORD (|(ASSIGN NUMBER));

commands: /* empty */
	| commands command SCOL
;

expIf: exp;

command: ID ASSIGN exp                                          # commandASSIGN
    | IF expIf THEN commands ifCommands END_IF                  # commandIF
    | WHILE expWhile DO commands END_WHILE                      # commandWHILE
;

expELSIF: exp;
expWhile: exp;

ifCommands: /* empty */                                         # commandEmpty
	| ELSIF expELSIF THEN commands ifCommands                   # commandELSIF
	| ELSE commands                                             # commandELSE
;



