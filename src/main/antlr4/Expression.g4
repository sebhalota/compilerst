grammar Expression;

exp : NUMBER                    # ExpNUMBER
	| ID                        # ExpID
	| TRUE                      # ExpTRUE
    | FALSE                     # ExpFALSE
	| OBRA exp CBRA             # ExpBRA
	| exp (MUL | DIV) exp       # ExpMUL_DIV
	| exp (ADD | SUB) exp       # ExpADD_SUB
	| exp EQ exp                # ExpEQ
	| exp NEQ exp               # ExpNEQ
	| exp LT exp                # ExpLT
	| exp LE exp                # ExpLE
	| exp GT exp                # ExpGT
    | exp GE exp                # ExpGE
	| exp AND exp               # ExpAND
	| exp OR exp                # ExpOR
;








