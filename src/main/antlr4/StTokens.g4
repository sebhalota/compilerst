lexer grammar StTokens;

// Program structure
VAR : 'VAR';
VAR_GLOBAL : 'VAR_GLOBAL';
VAR_INPUT : 'VAR_INPUT';
VAR_OUTPUT : 'VAR_OUTPUT';
VAR_IN_OUT : 'VAR_IN_OUT;';
END_VAR : 'END_VAR';
END_PROGRAM : 'END_PROGRAM';
PRG : 'PROGRAM';
FB : 'FUNCTION_BLOCK';
FC : 'FUNCTION';

// Data types
BOOL : 'BOOL';
BYTE : 'BYTE';
WORD : 'WORD';

// Operators
ASSIGN : ':=';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
EQ : '=';
NEQ : '<>';
LT : '<';
LE : '<=';
GT : '>';
GE : '>=';
OR : 'OR';
AND : 'AND';
XOR : 'XOR';

// Statement
IF : 'IF';
THEN : 'THEN';
ELSIF : 'ELSIF';
ELSE : 'ELSE';
END_IF : 'END_IF';
CASE : 'CASE';
OF : 'OF';
END_CASE : 'END_CASE';
WHILE : 'WHILE';
DO : 'DO';
END_WHILE : 'END_WHILE';

// Miscellaneous
AT : 'AT';
OUTPUT : '%QX';
INPUT : '%IX';
FALSE : 'FALSE';
TRUE : 'TRUE';
COL : ':';
SCOL : ';';
OBRA : '(';
CBRA : ')';
DOT : '.';
ID : [a-zA-Z]+ ; // match identifiers
NUMBER : [0-9]+ ; // match integers
//NEWLINE:'\r'?'\n' ; // return newlines to parser (is end-statement signal)
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
