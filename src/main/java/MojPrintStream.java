import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.util.Formatter;

public class MojPrintStream extends PrintStream {


    public static void generujBlad() {
        throw new IllegalStateException();
    }

    public static void aaaa() {
        try {
            generujBlad();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public MojPrintStream() {
        super(System.out);
    }

    @Override
    public void write(int b) {
        generujBlad();
    }

    @Override
    public void write(byte buf[], int off, int len) {
        generujBlad();
    }

//    @Override
//    private void write(char buf[]) {
//        generujBlad();
//    }

//    @Override
//    public void print(String s) {
//        aaaa();
//    }
//
//    @Override
//    public void println(String s) {
//        aaaa();
//    }
//
//    @Override
//    public void print(char x[]) {
//        aaaa();
//    }
//
//    @Override
//    public void println(char x[]) {
//        aaaa();
//    }
//
//    @Override
//    public void print(Object obj) {
//        aaaa();
//    }
//
//    @Override
//    public void println(Object obj) {
//        aaaa();
//    }

}
