package CaretPosition;

public class CaretPosition implements Cloneable {
    private int line;
    private int charPositionInLine;

    public CaretPosition() { }

    public CaretPosition(int line, int charPositionInLine) {
        this.line = line;
        this.charPositionInLine = charPositionInLine;
    }

    public int getLine() {
        return this.line;
    }

    public int getCharPositionInLine() {
        return this.charPositionInLine;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
