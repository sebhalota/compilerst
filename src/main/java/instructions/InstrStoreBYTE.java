package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.LDI;
import variables.Scope;
import variables.SymbolBYTE;
import variables.VariableStack;

public class InstrStoreBYTE extends Instruction {

    private int value;

    public InstrStoreBYTE(int value, CaretPosition caretPosition) {
        this.value = value;
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrStoreBYTE";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        variableStack.push(new SymbolBYTE("BYTE", value, this.caretPosition));
        VariableStack.Structure s = variableStack.peekOne();

        assemblerInstructionList.addAll(s.getSymbol().storeLiteralCode(s.getRegisterR(), value));

    }





}
