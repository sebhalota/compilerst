package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.IdNotDefinedError;
import errors.TypeMismatchError;
import instructions.Instruction;
import instructions.assembler.LDI;
import instructions.assembler.LDS;
import instructions.assembler.STS;
import variables.Symbol;
import variables.SymbolBOOL;
import variables.VariableStack;

public class InstrStoreVariable extends Instruction {

    private Symbol symbol;
    String variableName;

    public InstrStoreVariable(String variableName, CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
        this.variableName = variableName;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        symbol = symbolTable.find(variableName);

        if (symbol == null) {
            return new IdNotDefinedError(caretPosition, variableName);
        }

        return null;
    }

    @Override
    public String getName() {
        return "InstrStoreVariable";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        variableStack.push(symbol);
        VariableStack.Structure s = variableStack.peekOne();

        assemblerInstructionList.addAll(s.getSymbol().storeVariableCode(s.getRegisterR()));

    }

}
