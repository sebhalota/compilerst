package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.SeveralDeclarationError;
import errors.TypeMismatchError;
import errors.TypeMismatchInParaError;
import instructions.assembler.ADC;
import instructions.assembler.ADD;
import instructions.assembler.OR;
import variables.Symbol;
import variables.Type;
import variables.VariableStack;

public class InstrOR extends Instruction {

    public InstrOR(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        VariableStack.Structure[] symbols = variableStack.peek(2);

        if ((symbols[0].getSymbol().getType() == Type.BOOL) && (symbols[1].getSymbol().getType() != Type.BOOL)) {
            return new TypeMismatchInParaError(caretPosition, 2, "OR", symbols[0].getSymbol().getType().toString());
        }

        if ((symbols[0].getSymbol().getType() != Type.BOOL) && (symbols[1].getSymbol().getType() == Type.BOOL)) {
            return new TypeMismatchInParaError(caretPosition, 2, "OR", symbols[0].getSymbol().getType().toString());
        }

        return null;
    }

    @Override
    public String getName() {
        return "InstrOR";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        normalizeNumberDataTypes(variableStack);
        VariableStack.Structure[] symbols = variableStack.peek(2);
        variableStack.pop();

        for (int i = 0; i < symbols[0].getSymbol().size(); i++) {
            OR or = new OR((byte) (symbols[1].getRegisterR() + i), (byte) (symbols[0].getRegisterR() + i));
            assemblerInstructionList.add(or);
        }
    }

}
