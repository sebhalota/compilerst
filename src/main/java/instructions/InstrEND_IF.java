package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.TypeMismatchInParaError;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.Type;
import variables.VariableStack;

public class InstrEND_IF extends Instruction {

    public InstrEND_IF(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrEND_IF";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        NOP nop = new NOP(Label.Type.END_IF, getNextLabelName());
        assemblerInstructionList.add(nop);

    }

}
