package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.IdNotDefinedError;
import errors.TypeMismatchError;
import variables.*;

public class InstrASSIGN extends Instruction {

    private Symbol symbol;
    private String variableName;

    public InstrASSIGN(String variableName, CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
        this.variableName = variableName;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        symbol = symbolTable.find(variableName);
        VariableStack.Structure symbolOnStack = variableStack.peekOne();

        if (symbol == null) {
            return new IdNotDefinedError(caretPosition, variableName);
        } else if ((symbol.size() < symbolOnStack.getSymbol().size())
                || (symbol.getType() == Type.BOOL) && (symbolOnStack.getSymbol().getType() != Type.BOOL)
                || (symbol.getType() != Type.BOOL) && (symbolOnStack.getSymbol().getType() == Type.BOOL)
                ) {
            return new TypeMismatchError(caretPosition, symbolOnStack.getSymbol().getType().toString(), symbol.getType().toString());
        }
        return null;
    }

    @Override
    public String getName() {
        return "InstrASSIGN";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        VariableStack.Structure symbolOnStack = variableStack.peekOne();

        if (symbol.size() > symbolOnStack.getSymbol().size()) {
            changeLastVariableType(variableStack,symbol.getType());
        }

        assemblerInstructionList.addAll(symbol.assignCode(symbolOnStack.getRegisterR()));

        variableStack.pop();
    }

}
