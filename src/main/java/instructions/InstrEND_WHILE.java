package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.JMP;
import instructions.assembler.Jump;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.VariableStack;

public class InstrEND_WHILE extends Instruction {

    public InstrEND_WHILE(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrEND_WHILE";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        JMP jmp = new JMP(Jump.Type.WHILE_RETURN);
        assemblerInstructionList.add(jmp);

        NOP nop = new NOP(Label.Type.END_WHILE, getNextLabelName());
        assemblerInstructionList.add(nop);

    }

}
