package instructions;

import errors.CompilationError;
import instructions.assembler.LDI;
import variables.Symbol;
import variables.VariableStack;

public class InstrDeclaration extends Instruction {

    private int address;
    private int value;
    private int size;

    Symbol symbol;

    public InstrDeclaration(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public String getName() {
        return "InstrDeclaration";
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        assemblerInstructionList.addAll(symbol.declarationCode());

    }


}
