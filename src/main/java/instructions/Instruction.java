package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.AssemblerInstruction;
import instructions.assembler.LDI;
import instructions.assembler.MOV;
import variables.Type;
import variables.VariableStack;
import variables.SymbolTable;

import java.util.ArrayList;
import java.util.List;

public abstract class Instruction {

    protected CaretPosition caretPosition;
    protected List<AssemblerInstruction> assemblerInstructionList = new ArrayList<>();
    protected SymbolTable symbolTable;

    private static int labelName = 0;

    protected String getNextLabelName() {
        labelName++;
        return "s" + Integer.toString(labelName);
    }

    protected abstract CompilationError check(VariableStack variableStack);

    protected abstract String getName();

    protected abstract void generateCode(VariableStack variableStack);

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    protected void changeLastVariableType(VariableStack variableStack, Type type) {
        VariableStack.Structure symbolOnStack = variableStack.peekOne();
        int oldSize = symbolOnStack.getSymbol().size();

        variableStack.changeLastVariableType(type);
        symbolOnStack = variableStack.peekOne();


        for (int i = 0; (i < symbolOnStack.getSymbol().size() - oldSize); i++) {
            LDI ldi = new LDI((byte) 0, (byte) (symbolOnStack.getRegisterR() + oldSize + i));
            assemblerInstructionList.add(ldi);
        }
    }

    protected void changePenultVariableType(VariableStack variableStack, Type type) {
        VariableStack.Structure[] symbols = variableStack.peek(2);

        if (symbols[1].getSymbol().getType() != type) {

            int oldRegisterR = symbols[0].getRegisterR();
            int oldSize = symbols[1].getSymbol().size();

            variableStack.changePenultVariableType(type);

            symbols = variableStack.peek(2);

            for (int i = 0; i < symbols[0].getSymbol().size(); i++) {
                byte b1 = (byte) (symbols[0].getRegisterR() + symbols[0].getSymbol().size() - 1 - i);
                byte b2 = (byte) (oldRegisterR + symbols[0].getSymbol().size() - 1 - i);
                MOV mov = new MOV(b1, b2);
                assemblerInstructionList.add(mov);
            }

            for (int i = 0; i < (symbols[1].getSymbol()).size() - oldSize; i++) {
                LDI ldi = new LDI((byte) 0, (byte) (symbols[1].getRegisterR() + oldSize + i));
                assemblerInstructionList.add(ldi);
            }

        }
    }

    protected void normalizeNumberDataTypes(VariableStack variableStack) {

        VariableStack.Structure[] symbols = variableStack.peek(2);

        if (symbols[0].getSymbol().size() < symbols[1].getSymbol().size()) {
            System.out.println("1");
            changeLastVariableType(variableStack, symbols[1].getSymbol().getType());
        } else if (symbols[0].getSymbol().size() > symbols[1].getSymbol().size()) {
            System.out.println("2");
            changePenultVariableType(variableStack, symbols[0].getSymbol().getType());
        }

        symbols = variableStack.peek(2);

    }

    public String getAssemblerCode() {
        StringBuilder str = new StringBuilder();
        str.append("//" + getName() + "\n");
        for (AssemblerInstruction a : assemblerInstructionList) {
            str.append(a.getAssemblerCode());
            str.append("\n");
        }
        str.append("\n");
        return str.toString();
    }

    public int getCodeSize() {
        int codeSize = 0;

        for (AssemblerInstruction a : assemblerInstructionList) {
            codeSize += a.getSizeInBytes();
        }

        return codeSize;
    }


}
