package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.VariableStack;

public class InstrEndDeclarations extends Instruction {

    public InstrEndDeclarations(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrEndDeclarations";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        NOP nop = new NOP(Label.Type.PROGRAM_START, getNextLabelName());
        assemblerInstructionList.add(nop);
    }

}
