package instructions;

import errors.CompilationError;
import errors.CompilationErrorList;
import instructions.assembler.AssemblerInstruction;
import variables.Symbol;
import variables.SymbolTable;
import variables.VariableStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstructionList {

    private List<Instruction> list = new ArrayList<>();
    private VariableStack variableStack = new VariableStack();
    private CompilationErrorList compilationErrorList;
    private SymbolTable symbolTable;
    private Backpatching backpatching = new Backpatching();

    public InstructionList(CompilationErrorList compilationErrorList, SymbolTable symbolTable) {
        this.compilationErrorList = compilationErrorList;
        this.symbolTable = symbolTable;
    }

    public void add(Instruction instruction) {
        instruction.setSymbolTable(symbolTable);

        if (compilationErrorList.noErrors()) {
            CompilationError compilationError = instruction.check(variableStack);
            if (compilationError == null) {
                instruction.generateCode(variableStack);
                this.list.add(instruction);
            }
            else {
                compilationErrorList.add(compilationError);
            }
        }

    }

    public String generateHex() {
        HexGenerator hexGenerator = new HexGenerator();

        for(Instruction i:list) {
            for(AssemblerInstruction a:i.assemblerInstructionList) {
                hexGenerator.add(a.opcodeGenerator.getOpcodeSize(),a.opcodeGenerator.getOpcode());
            }
        }

        return hexGenerator.getHex();
    }

    public String generateAssemblerCode() {
        StringBuilder str = new StringBuilder();
        for (Instruction l : list) {
            str.append(l.getAssemblerCode());
        }
        return str.toString();
    }

    public void makeBackpatching() {
        backpatching.make(list);
    }

    public int getCodeSize() {
        int codeSize = 0;

        for(Instruction l:list) {
            codeSize += l.getCodeSize();
        }

        return codeSize;
    }



}
