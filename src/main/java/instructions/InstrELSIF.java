package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.RequireBOOLexpError;
import errors.TypeMismatchInParaError;
import instructions.assembler.*;
import variables.SymbolBOOL;
import variables.Type;
import variables.VariableStack;

public class InstrELSIF extends Instruction {

    public InstrELSIF(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        VariableStack.Structure s = variableStack.peekOne();
        if(s.getSymbol().getType() != Type.BOOL) {
            return new RequireBOOLexpError(this.caretPosition,"ELSIF");
        }
        return null;
    }

    @Override
    public String getName() {
        return "InstrELSIF";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        variableStack.push(new SymbolBOOL("BOOL",false, this.caretPosition));
        VariableStack.Structure[] s = variableStack.peek(2);

        JMP jmp = new JMP(Jump.Type.IF_SUCCESS);
        assemblerInstructionList.add(jmp);

        NOP nop = new NOP(Label.Type.IF_CHOICE,getNextLabelName());
        assemblerInstructionList.add(nop);

        LDI ldi = new LDI((byte) 1,s[0].getRegisterR());
        assemblerInstructionList.add(ldi);

        CP cp = new CP(s[0].getRegisterR(),s[1].getRegisterR());
        assemblerInstructionList.add(cp);

        BRNE brne = new BRNE(Jump.Type.ELSIF);
        assemblerInstructionList.add(brne);

        variableStack.pop();
        variableStack.pop();
    }

}
