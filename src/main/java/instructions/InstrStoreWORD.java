package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.LDI;
import variables.SymbolWORD;
import variables.VariableStack;

public class InstrStoreWORD extends Instruction {

    private int value;

    public InstrStoreWORD(int value, CaretPosition caretPosition) {
        this.value = value;
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrStoreWORD";
    }

    public void generateCode(VariableStack variableStack) {
        variableStack.push(new SymbolWORD("WORD",value,this.caretPosition));
        VariableStack.Structure s = variableStack.peekOne();

        assemblerInstructionList.addAll(s.getSymbol().storeLiteralCode(s.getRegisterR(), value));

    }






}
