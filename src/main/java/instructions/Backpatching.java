package instructions;

import instructions.assembler.AssemblerInstruction;
import instructions.assembler.Jump;
import instructions.assembler.Jumpable;
import instructions.assembler.Label;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Backpatching {

    private class Structure {
        Jump.Type jumpType;
        Jump.Type startNewJumpType;
        Label.Type endLabelType;
        Label.Type[] termLabelTypes;
        boolean desc;

        private Structure(Jump.Type jumpType, Jump.Type startNewJumpType, Label.Type endLabelType, Label.Type[] termLabelTypes, boolean desc) {
            this.jumpType = jumpType;
            this.startNewJumpType = startNewJumpType;
            this.endLabelType = endLabelType;
            this.termLabelTypes = termLabelTypes;
            this.desc = desc;
        }
    }

    private Structure[] formula = new Structure[]{
            new Structure(Jump.Type.IF, Jump.Type.IF, Label.Type.END_IF, new Label.Type[]{Label.Type.IF_CHOICE, Label.Type.END_IF}, false),
            new Structure(Jump.Type.ELSIF, Jump.Type.IF, Label.Type.END_IF, new Label.Type[]{Label.Type.IF_CHOICE, Label.Type.END_IF}, false),
            new Structure(Jump.Type.IF_SUCCESS, Jump.Type.IF, Label.Type.END_IF, new Label.Type[]{Label.Type.END_IF}, false),
            new Structure(Jump.Type.WHILE, Jump.Type.WHILE, Label.Type.END_WHILE, new Label.Type[]{Label.Type.END_WHILE}, false),
            new Structure(Jump.Type.WHILE_RETURN, Jump.Type.WHILE_RETURN, Label.Type.WHILE_START, new Label.Type[]{Label.Type.WHILE_START}, true),
            new Structure(Jump.Type.PROGRAM_RETURN, Jump.Type.PROGRAM_RETURN, Label.Type.PROGRAM_START,new Label.Type[]{Label.Type.PROGRAM_START},true)
    };

    public void make(List<Instruction> list) {
        for (int i = 0; i < list.size(); i++) {
            for (AssemblerInstruction a : list.get(i).assemblerInstructionList) {
                for (Structure f : formula) {
                    if (a instanceof Jumpable) {

                        if (((Jumpable) a).getJumpType() == f.jumpType) {
                            ((Jumpable) a).setJumpLabel(find(list, i, f.startNewJumpType, f.endLabelType, f.termLabelTypes, f.desc));
                        }
                    }
                }
            }
        }

        Map<String, Integer> map = labelsAddress(list);
        saveAddresses(list, map);
    }

    private String find(List<Instruction> list, int startIndex, Jump.Type startNewJumpType, Label.Type endLabelType, Label.Type[] termLabelTypes, boolean desc) {
        int x = 0;

        int i;
        if (desc) i = startIndex - 1;
        else i = startIndex + 1;

        while (i >= 0 && i < list.size()) {
            for (AssemblerInstruction a : list.get(i).assemblerInstructionList) {

                if (a instanceof Jumpable) {
                    if (((Jumpable) a).getJumpType() == startNewJumpType) x++;
                }

                if (x == 0) {
                    for (Label.Type t : termLabelTypes) {
                        if (a.label.getLabelType() == t) {
                            return a.label.getLabelName();
                        }
                    }
                }
                if (a.label.getLabelType() == endLabelType) x--;
            }

            if (desc) i--;
            else i++;
        }

        return "";
    }

    private void saveAddresses(List<Instruction> list, Map<String, Integer> map) {

        for (Instruction l : list) {
            for (AssemblerInstruction a : l.assemblerInstructionList) {
                if (a instanceof Jumpable) {
                    ((Jumpable) a).setJumpAddress(map.get(((Jumpable) a).getJumpLabel()));
                }
            }
        }
    }

    private Map<String, Integer> labelsAddress(List<Instruction> list) {

        Map<String, Integer> map = new TreeMap<>();

        for (Instruction l : list) {
            for (AssemblerInstruction a : l.assemblerInstructionList) {
                if (a.label.getLabelName().length() > 0) {
                    map.put(a.label.getLabelName(), a.getAddress());
                }
            }
        }

        return map;
    }

}
