package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.TypeMismatchInParaError;
import instructions.assembler.*;
import variables.SymbolBOOL;
import variables.Type;
import variables.VariableStack;

public class InstrNEQ extends Instruction {

    public InstrNEQ(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        VariableStack.Structure[] symbols = variableStack.peek(2);

        if ((symbols[0].getSymbol().getType() == Type.BOOL) && (symbols[1].getSymbol().getType() != Type.BOOL)) {
            return new TypeMismatchInParaError(caretPosition, 2, "<>", symbols[0].getSymbol().getType().toString());
        }
        if ((symbols[0].getSymbol().getType() != Type.BOOL) && (symbols[1].getSymbol().getType() == Type.BOOL)) {
            return new TypeMismatchInParaError(caretPosition, 2, "<>", symbols[0].getSymbol().getType().toString());
        }
        return null;
    }

    @Override
    public String getName() {
        return "InstrNEQ";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        normalizeNumberDataTypes(variableStack);
        VariableStack.Structure[] symbols = variableStack.peek(2);
        variableStack.pop();
        variableStack.pop();
        variableStack.push(new SymbolBOOL("BOOL", false, symbols[1].getSymbol().getCaretPosition()));
        VariableStack.Structure resultDest = variableStack.peekOne();

        for (int i = 0; i < symbols[0].getSymbol().size(); i++) {
            if (i == 0) {
                CP cp = new CP(symbols[0].getRegisterR(), symbols[1].getRegisterR());
                assemblerInstructionList.add(cp);
            } else {
                CPC cpc = new CPC((byte) (symbols[0].getRegisterR() + i), (byte) (symbols[1].getRegisterR() + i));
                assemblerInstructionList.add(cpc);
            }
        }

        NOP nop = new NOP(Label.Type.EMPTY, getNextLabelName());
        LDI ldi1 = new LDI((byte) 0, resultDest.getRegisterR());
        BREQ breq = new BREQ(nop.label.getLabelName());
        LDI ldi2 = new LDI((byte) 1, resultDest.getRegisterR());

        assemblerInstructionList.add(ldi1);
        assemblerInstructionList.add(breq);
        assemblerInstructionList.add(ldi2);
        assemblerInstructionList.add(nop);

    }

}
