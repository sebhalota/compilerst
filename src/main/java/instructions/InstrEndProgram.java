package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.JMP;
import instructions.assembler.Jump;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.VariableStack;

public class InstrEndProgram extends Instruction {

    public InstrEndProgram(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrEndProgram";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        JMP jmp = new JMP(Jump.Type.PROGRAM_RETURN);
        assemblerInstructionList.add(jmp);

    }

}
