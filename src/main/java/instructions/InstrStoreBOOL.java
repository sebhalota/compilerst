package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import instructions.assembler.LDI;
import variables.*;

public class InstrStoreBOOL extends Instruction {

    private boolean value;

    public InstrStoreBOOL(boolean value, CaretPosition caretPosition) {
        this.value = value;
        this.caretPosition = caretPosition;
    }

    public void modifyStack(VariableStack variableStack) {
        variableStack.push(new SymbolBOOL("BOOL", value, this.caretPosition));
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrStoreBOOL";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        variableStack.push(new SymbolBOOL("BOOL", value, this.caretPosition));
        VariableStack.Structure s = variableStack.peekOne();

//        LDI ldi;
//        if (value) ldi = new LDI((byte) 1,s.getRegisterR());
//        else ldi = new LDI((byte) 0, s.getRegisterR());
//        assemblerInstructionList.add(ldi);

        assemblerInstructionList.addAll(s.getSymbol().storeLiteralCode(s.getRegisterR(), value ? 1 : 0));

    }


}
