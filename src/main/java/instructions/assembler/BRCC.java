package instructions.assembler;

public class BRCC extends AssemblerInstruction implements Jumpable {

    private static final String pattern = "111101AAAAAAA000";
    public Jump jump = new Jump();

    public BRCC(String jumpLable) {
        super(pattern,0,0);
        jump.setJumpLabel(jumpLable);
    }

    public BRCC(Jump.Type jumpType) {
        super(pattern,0,0);
        jump.setJumpType(jumpType);
    }

    @Override
    protected String getName() {
        return "BRCC offset:" + jump.getJumpAddress();
    }

    @Override
    public String getAssemblerCodeImpl() {
        return "brcc " + jump.getJumpLabel();
    }

    @Override
    public Jump.Type getJumpType() {
        return jump.getJumpType();
    }

    @Override
    public void setJumpType(Jump.Type jumpType) {
        jump.setJumpType(jumpType);
    }

    @Override
    public int getJumpAddress() {
        return this.getJumpAddress();
    }

    @Override
    public void setJumpAddress(int jumpAddress) {
        jump.setJumpAddress(jumpAddress - getAddress()-1);
        opcodeGenerator.setArgA(jump.getJumpAddress());
    }

    @Override
    public String getJumpLabel() {
        return jump.getJumpLabel();
    }

    @Override
    public void setJumpLabel(String jumpLabel) {
        jump.setJumpLabel(jumpLabel);
    }


}
