package instructions.assembler;

public abstract class AssemblerInstruction {

    public Label label;
    public OpcodeGenerator opcodeGenerator;
    private int address;
    private int sizeInBytes;

    private static int freeAddress = 0;

    public AssemblerInstruction(String pattern, int a, int b) {
        label = new Label();
        opcodeGenerator = new OpcodeGenerator(pattern, a, b);
        address = freeAddress;
        increaseFreeAddress(opcodeGenerator.getOpcodeSize());
        setSizeInBytes(opcodeGenerator.getOpcodeSize());
    }

    private void increaseFreeAddress(int opcodeSize) {
        if (opcodeSize == 16) freeAddress += 1;
        else if (opcodeSize == 32) freeAddress += 2;
    }

    private void setSizeInBytes(int opcodeSize) {
        if (opcodeSize == 16) sizeInBytes = 2;
        else if (opcodeSize == 32) sizeInBytes = 4;
    }

    public int getSizeInBytes() {
        return this.sizeInBytes;
    }

    public int getAddress() {
        return this.address;
    }

    public String getAssemblerCode() {

        if (label.getLabelName().length() > 0) {
            return label.getLabelName() + ": " + getAssemblerCodeImpl();
        } else return getAssemblerCodeImpl();
    }

    protected abstract String getName();

    protected abstract String getAssemblerCodeImpl();

}
