package instructions.assembler;

public class JMP extends AssemblerInstruction implements Jumpable {

    private static final String pattern = "1001010AAAAA110AAAAAAAAAAAAAAAAA";
    private Jump jump = new Jump();

    public JMP(int jumpAddress) {
        super(pattern,jumpAddress,0);
        jump.setJumpAddress(jumpAddress);
    }

    public JMP(Jump.Type jumpType) {
        super(pattern,0,0);
        jump.setJumpType(jumpType);
    }

    @Override
    protected String getName() {
        return "jmp address:" + jump.getJumpAddress();
    }

    @Override
    public String getAssemblerCodeImpl() {
        return "jmp " + jump.getJumpLabel();
    }

    @Override
    public Jump.Type getJumpType() {
        return jump.getJumpType();
    }

    @Override
    public void setJumpType(Jump.Type jumpType) {
        jump.setJumpType(jumpType);
    }

    @Override
    public int getJumpAddress() {
        return this.getJumpAddress();
    }

    @Override
    public void setJumpAddress(int jumpAddress) {
        jump.setJumpAddress(jumpAddress);
        opcodeGenerator.setArgA(jumpAddress);
    }

    @Override
    public String getJumpLabel() {
        return jump.getJumpLabel();
    }

    @Override
    public void setJumpLabel(String jumpLabel) {
        jump.setJumpLabel(jumpLabel);
    }


}
