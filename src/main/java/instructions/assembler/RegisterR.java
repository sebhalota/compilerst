package instructions.assembler;

public class RegisterR {

    private byte value;

    public RegisterR(byte value) {
        if(!(value >= 16 && value <=31)) {
            throw new IllegalArgumentException("registerR should be between 16 and 32");
        }

        this.value = value;
    }

    public byte getValue() {
        return value;
    }
}
