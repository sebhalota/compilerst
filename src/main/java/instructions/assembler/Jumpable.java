package instructions.assembler;

public interface Jumpable {

    Jump.Type getJumpType();
    void setJumpType(Jump.Type jumpType);
    int getJumpAddress();
    void setJumpAddress(int jumpAddress);
    String getJumpLabel();
    void setJumpLabel(String jumpLabel);

}
