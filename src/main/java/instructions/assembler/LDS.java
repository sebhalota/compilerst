package instructions.assembler;

public class LDS extends AssemblerInstruction {

    private static final String pattern = "1001000AAAAA0000BBBBBBBBBBBBBBBB";

    private byte register;
    private short dataSpace;

    public LDS(byte register, short dataSpace) {
        super(pattern, register, dataSpace);
        this.register = register;
        this.dataSpace = dataSpace;
    }

    @Override
    protected String getName() {
        return "LDS register:" + register + " dataSpace:" + dataSpace;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("lds r%d,0x%04X", register, dataSpace);
    }
}

