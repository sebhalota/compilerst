package instructions.assembler;

public class SBRS extends AssemblerInstruction {

    private static final String pattern = "1111111AAAAA0BBB";

    private byte register;
    private byte bit;

    public SBRS(byte register,byte bit) {
        super(pattern,register,bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "SBRS register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbrs r%d,%d", register, bit);
    }

}
