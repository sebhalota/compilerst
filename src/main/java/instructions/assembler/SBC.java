package instructions.assembler;

// 0
public class SBC extends AssemblerInstruction {

    private static final String pattern = "000010ABBBBBAAAA";

    private byte register1;
    private byte register2;

    public SBC(byte register1, byte register2) {
        super(pattern,register2,register1);
        this.register1 = register1;
        this.register2 = register2;
    }

    @Override
    protected String getName() {
        return "SBC register1:" + register1 + " register2:" + register2;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbc r%d,r%d", register1, register2);
    }
}
