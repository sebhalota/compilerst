package instructions.assembler;


//0
public class CP extends AssemblerInstruction {

    private static final String pattern = "000101ABBBBBAAAA";

    private byte register1;
    private byte register2;

    public CP(byte register1, byte register2) {
        super(pattern, register2, register1);
        this.register1 = register1;
        this.register2 = register2;
    }

    @Override
    protected String getName() {
        return "CP register1:" + register1 + " register2:" + register2;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("cp r%d,r%d", register1, register2);
    }
}
