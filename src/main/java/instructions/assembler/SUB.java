package instructions.assembler;

// 0
public class SUB extends AssemblerInstruction {

    private static final String pattern = "000110ABBBBBAAAA";

    private byte register1;
    private byte register2;

    public SUB(byte register1, byte register2) {
        super(pattern,register2,register1);
        this.register1 = register1;
        this.register2 = register2;
    }

    @Override
    protected String getName() {
        return "SUB register1:" + register1 + " register2:" + register2;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sub r%d,r%d", register1, register2);
    }
}
