package instructions.assembler;

// 0
public class LDI extends AssemblerInstruction {

    private static final String pattern = "1110AAAABBBBAAAA";

    private byte constant;
    private byte register;

    public LDI(byte constant, byte register) {
        super(pattern,constant,register);
        this.constant = constant;
        this.register = register;
    }

    @Override
    protected String getName() {
        return "LDI constant:" + constant +" register:"+register;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("ldi r%d,0x%02X",register,constant);
    }


}
