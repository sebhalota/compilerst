package instructions.assembler;

public class AND extends AssemblerInstruction {

    private static final String pattern = "001000ABBBBBAAAA";

    private RegisterR registerR1;
    private RegisterR registerR2;

    public AND(byte register1, byte register2) {
        super(pattern, register2, register1);
        registerR1 = new RegisterR(register1);
        registerR2 = new RegisterR(register2);
    }

    @Override
    protected String getName() {
        return "AND register1:" + registerR1.getValue() + " register2:" + registerR2.getValue();
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("and r%d,r%d", registerR1.getValue(), registerR2.getValue());
    }

}
