package instructions.assembler;

public class SBIS extends AssemblerInstruction {

    private static final String pattern = "10011011AAAAABBB";

    private byte register;
    private byte bit;

    public SBIS(byte register, byte bit) {
        super(pattern,register,bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "SBIS register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbis 0x%02X,%d", register, bit);
    }



}
