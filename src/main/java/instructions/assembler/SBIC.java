package instructions.assembler;

public class SBIC extends AssemblerInstruction {

    private static final String pattern = "10011001AAAAABBB";

    private byte register;
    private byte bit;

    public SBIC(byte register, byte bit) {
        super(pattern, register, bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "SBIC + register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbic 0x%02X,%d", register, bit);
    }
}
