package instructions.assembler;

public class BRGE extends AssemblerInstruction implements Jumpable {

    private static final String pattern = "111101AAAAAAA100";
    private Jump jump = new Jump();

    public BRGE(String jumpLable) {
        super(pattern,0,0);
        jump.setJumpLabel(jumpLable);
    }

    public BRGE(Jump.Type jumpType) {
        super(pattern,0,0);
        jump.setJumpType(jumpType);
    }

    @Override
    protected String getName() {
        return "BRGE offset:" + jump.getJumpAddress();
    }

    @Override
    public String getAssemblerCodeImpl() {
        return "brge " + jump.getJumpLabel();
    }

    @Override
    public Jump.Type getJumpType() {
        return jump.getJumpType();
    }

    @Override
    public void setJumpType(Jump.Type jumpType) {
        jump.setJumpType(jumpType);
    }

    @Override
    public int getJumpAddress() {
        return this.getJumpAddress();
    }

    @Override
    public void setJumpAddress(int jumpAddress) {
        jump.setJumpAddress(jumpAddress - getAddress()-1);
        opcodeGenerator.setArgA(jump.getJumpAddress());
    }

    @Override
    public String getJumpLabel() {
        return jump.getJumpLabel();
    }

    @Override
    public void setJumpLabel(String jumpLabel) {
        jump.setJumpLabel(jumpLabel);
    }


}
