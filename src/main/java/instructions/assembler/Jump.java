package instructions.assembler;

public class Jump implements Jumpable{

    public enum Type {
        EMPTY,
        IF,             // jump to IF_CHOICE, END_IF
        IF_SUCCESS,     // jump to END_IF
        ELSIF,          // jump to IF_CHOICE, END_IF
        WHILE,          // jump to END_WHILE
        WHILE_RETURN,   // jump to WHILE_START
        PROGRAM_RETURN
    }

    private Type jumpType = Type.EMPTY;
    private int jumpAddress = 0;
    private String jumpLabel = "";

    public Type getJumpType() {
        return this.jumpType;
    }

    public void setJumpType(Type jumpType) {
        this.jumpType = jumpType;
    }

    public int getJumpAddress() {
        return this.jumpAddress;
    }

    public void setJumpAddress(int jumpAddress) {
        this.jumpAddress = jumpAddress;
    }

    public String getJumpLabel() {
        return this.jumpLabel;
    }

    public void setJumpLabel(String jumpLabel) {
        this.jumpLabel = jumpLabel;
    }

}
