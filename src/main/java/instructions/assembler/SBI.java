package instructions.assembler;

public class SBI extends AssemblerInstruction {

    private static final String pattern = "10011010AAAAABBB";

    private byte register;
    private byte bit;

    public SBI(byte register, byte bit) {
        super(pattern,register,bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "SBI + register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbi 0x%02X,%d", register, bit);
    }
}
