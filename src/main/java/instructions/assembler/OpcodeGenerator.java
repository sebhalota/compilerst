package instructions.assembler;

public class OpcodeGenerator {

    private String pattern;
    private int a;
    private int b;
    private int opcodeSize;

    public OpcodeGenerator(String pattern, int a, int b) {

        if(pattern == null)
            throw new IllegalArgumentException("Pattern cannot be null.");

        if(!patternLengthIsCorrect(pattern))
            throw new IllegalArgumentException("Pattern must be 16 or 32 length.");

        if(!patternContainsValidCharacters(pattern))
            throw new IllegalArgumentException("Pattern must contain only the characters '0','1','A','B'");

        this.opcodeSize = countOppCodeSize(pattern);
        this.pattern = pattern;
        this.a = a;
        this.b = b;
    }

    private static boolean patternLengthIsCorrect(String pattern) {
        return (pattern.length()==16 || pattern.length()==32);
    }

    private static boolean patternContainsValidCharacters(String pattern) {
        for(char c:pattern.toCharArray()) {
            if(!(c== '0' || c=='1' || c=='A' || c== 'B')) return false;
        }
        return true;
    }

    public void setArgA(int a) {
        this.a = a;
    }

    public void setArgB(int b) {
        this.b = b;
    }

    public int getOpcode() {
        return convertToLittleEndian(generateOpcode(pattern, a, b));
    }

    public int getOpcodeSize() {
        return this.opcodeSize;
    }

    private int generateOpcode(String pattern, int a, int b) {
        int code = 0;

        if (pattern.length() == 16 || pattern.length() == 32) {
            char[] arrPattern = reverse(pattern.toCharArray());
            int aCounter = 0;
            int bCounter = 0;

            for (int i = 0; i < pattern.length(); i++) {

                switch (arrPattern[i]) {
                    case '0':
                        break;

                    case '1':
                        code |= (1 << i);
                        break;

                    case 'A':
                        code |= ((a & (1 << aCounter)) >> aCounter) << i;
                        aCounter++;
                        break;

                    case 'B':
                        code |= ((b & (1 << bCounter)) >> bCounter) << i;
                        bCounter++;
                        break;
                }
            }
        }

        return code;
    }

    private int countOppCodeSize(String pattern) {
        if (pattern.length() == 16) return 16;
        else if (pattern.length() == 32) return 32;
        else return 0;
    }

    private char[] reverse(char[] x) {
        char[] y = new char[x.length];

        for (int i = 0; i < y.length; i++) {
            y[y.length - i - 1] = x[i];
        }

        return y;
    }

    private int convertToLittleEndian(int x) {
        int y = 0;

        y |= (x & 0xFF000000) >>> 8;
        y |= (x & 0x00FF0000) << 8;
        y |= (x & 0x0000FF00) >>> 8;
        y |= (x & 0x000000FF) << 8;

        return y;
    }

}
