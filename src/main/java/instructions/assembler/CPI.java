package instructions.assembler;

public class CPI extends AssemblerInstruction  {

    private static final String pattern = "0011AAAABBBBAAAA";

    private byte register;
    private byte constant;

    public CPI(byte register,byte constant) {
        super(pattern,constant,register);
        this.register = register;
        this.constant = constant;
    }

    @Override
    protected String getName() {
        return "CPI register:" + register + " constant" + constant;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("cpi r%d,r%d", register, constant);
    }




}
