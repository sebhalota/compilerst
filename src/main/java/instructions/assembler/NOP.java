package instructions.assembler;

public class NOP extends AssemblerInstruction {

    private static final String pattern = "0000000000000000";

    public NOP() {
        super(pattern,0,0);
    }

    public NOP(Label.Type labelType, String labelName) {
        super(pattern,0,0);
        super.label.setLabelType(labelType);
        super.label.setLabelName(labelName);
    }

    @Override
    protected String getName() {
        return "nop";
    }

    @Override
    public String getAssemblerCodeImpl() {
        return "nop";
    }

}
