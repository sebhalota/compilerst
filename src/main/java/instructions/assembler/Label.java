package instructions.assembler;

public class Label {

    public enum Type {
        EMPTY,
        IF_CHOICE,
        END_IF,
        WHILE_START,
        END_WHILE,
        PROGRAM_START
    }

    private String labelName = "";
    private Label.Type labelType = Type.EMPTY;

    public String getLabelName() {
        return this.labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public Type getLabelType() {
        return this.labelType;
    }

    public void setLabelType(Type labelType) {
        this.labelType = labelType;
    }

}
