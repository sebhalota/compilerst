package instructions.assembler;

public class MOV extends AssemblerInstruction {

    private static final String pattern = "001011ABBBBBAAAA";

    private byte register1;
    private byte register2;

    public MOV(byte register1,byte register2) {
        super(pattern,register2,register1);
        this.register1 = register1;
        this.register2 = register2;
    }

    @Override
    protected String getName() {
        return "MOV register1:" + register1 + " register2:" + register2;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("mov r%d,r%d", register1, register2);
    }

}
