package instructions.assembler;

public class SBRC extends AssemblerInstruction {

    private static final String pattern = "1111110AAAAA0BBB";

    private byte register;
    private byte bit;

    public SBRC(byte register,byte bit) {
        super(pattern,register,bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "SBRC register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sbrc r%d,%d", register, bit);
    }

}
