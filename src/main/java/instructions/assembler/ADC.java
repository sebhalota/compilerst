package instructions.assembler;

public class ADC extends AssemblerInstruction {

    private static final String pattern = "000111ABBBBBAAAA";

    private RegisterR registerR1;
    private RegisterR registerR2;

    public ADC(byte register1, byte register2) {
        super(pattern, register2, register1);
        this.registerR1 = new RegisterR(register1);
        this.registerR2 = new RegisterR(register2);
    }

    @Override
    protected String getName() {
        return "ADC register1:" + registerR1.getValue() + " register2:" + registerR2.getValue();
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("adc r%d,r%d", registerR1.getValue(), registerR2.getValue());
    }

}
