package instructions.assembler;

// 0
public class CBI extends AssemblerInstruction {

    private static final String pattern = "10011000AAAAABBB";

    private byte register;
    private byte bit;

    public CBI(byte register, byte bit) {
        super(pattern, register, bit);
        this.register = register;
        this.bit = bit;
    }

    @Override
    protected String getName() {
        return "CBI register:" + register + " bit:" + bit;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("cbi 0x%02X,%d", register, bit);
    }


}
