package instructions.assembler;

// 1
public class STS extends AssemblerInstruction {

    private static final String pattern = "1001001AAAAA0000BBBBBBBBBBBBBBBB";

    private byte register;
    private short dataSpace;

    public STS(byte register, short dataSpace) {
        super(pattern,register,dataSpace);
        this.register = register;
        this.dataSpace = dataSpace;
    }

    @Override
    protected String getName() {
        return "STS register:" + register + " dataSpace:" + dataSpace;
    }

    @Override
    public String getAssemblerCodeImpl() {
        return String.format("sts 0x%04X, r%d",dataSpace,register);
    }
}
