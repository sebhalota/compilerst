package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.RequireBOOLexpError;
import errors.TypeMismatchInParaError;
import instructions.assembler.BRNE;
import instructions.assembler.CP;
import instructions.assembler.Jump;
import instructions.assembler.LDI;
import variables.SymbolBOOL;
import variables.Type;
import variables.VariableStack;

public class InstrIF extends Instruction {

    public InstrIF(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        VariableStack.Structure s = variableStack.peekOne();
        if (s.getSymbol().getType() != Type.BOOL) {
            return new RequireBOOLexpError(this.caretPosition, "IF");
        }
        return null;
    }

    @Override
    public String getName() {
        return "InstrIF";
    }

    @Override
    public void generateCode(VariableStack variableStack) {
        variableStack.push(new SymbolBOOL("BOOL", false, this.caretPosition));
        VariableStack.Structure[] s = variableStack.peek(2);

        LDI ldi = new LDI((byte) 1, s[0].getRegisterR());
        assemblerInstructionList.add(ldi);

        CP cp = new CP(s[0].getRegisterR(), s[1].getRegisterR());
        assemblerInstructionList.add(cp);

        BRNE brne = new BRNE(Jump.Type.IF);
        assemblerInstructionList.add(brne);

        variableStack.pop();
        variableStack.pop();

    }


}
