package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.TypeMismatchInParaError;
import instructions.assembler.ADC;
import instructions.assembler.ADD;
import instructions.assembler.SBC;
import instructions.assembler.SUB;
import variables.Type;
import variables.VariableStack;

public class InstrSUB extends Instruction {

    public InstrSUB(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        VariableStack.Structure[] symbols = variableStack.peek(2);

        if (symbols[0].getSymbol().getType() == Type.BOOL) {
            return new TypeMismatchInParaError(caretPosition, 1, "-", symbols[0].getSymbol().getType().toString());
        }
        if (symbols[1].getSymbol().getType() == Type.BOOL) {
            return new TypeMismatchInParaError(caretPosition, 2, "-", symbols[1].getSymbol().getType().toString());
        }
        return null;
    }

    @Override
    public String getName() {
        return "InstrSUB";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        normalizeNumberDataTypes(variableStack);
        VariableStack.Structure[] symbols = variableStack.peek(2);
        variableStack.pop();

        for (int i = 0; i < symbols[0].getSymbol().size(); i++) {
            if (i == 0) {
                SUB sub = new SUB((byte) (symbols[1].getRegisterR() + i), (byte) (symbols[0].getRegisterR() + i));
                assemblerInstructionList.add(sub);
            } else {
                SBC sbc = new SBC((byte) (symbols[1].getRegisterR() + i), (byte) (symbols[0].getRegisterR() + i));
                assemblerInstructionList.add(sbc);
            }
        }
    }

}
