package instructions;

import java.util.ArrayList;
import java.util.List;

public class HexGenerator {

    private String preamble = ":020000020000FC\n";
    private String ending = ":00000001FF\n";

    private class Structure {
        private int opcodeSize;
        private int opcode;

        private Structure(int opcodeSize, int opcode) {
            this.opcodeSize = opcodeSize;
            this.opcode = opcode;
        }

        private String getOpcodeString() {
            if (opcodeSize == 16)
                return String.format("%04X", opcode);
            else if (opcodeSize == 32)
                return String.format("%08X", opcode);
            else
                return "zle";
        }
    }

    private List<Structure> list = new ArrayList<>();

    public void add(int opcodeSize, int opcode) {
        Structure structure = new Structure(opcodeSize,opcode);
        list.add(structure);
    }

    public String getHex() {

        StringBuilder hex = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        int address = 0;

        hex.append(preamble);
        for(Structure s:list) {
            temp.append(s.getOpcodeString());
            if(temp.length() >= 32) {
                hex.append(hexLine(address, 0, temp.substring(0, 32)));
                temp = new StringBuilder(temp.substring(32));
                address += 16;
            }
        }
        hex.append(hexLine(address, 0, temp.toString()));
        hex.append(ending);

        return hex.toString();
    }

    private StringBuilder hexLine(int address, int recordType, String data) {
        StringBuilder str = new StringBuilder();
        str.append(String.format("%02X%04X%02X", data.length() / 2, address, recordType));
        str.append(data);
        str.append(checksum(str));
        str.append("\n");
        str.insert(0, ":");
        return str;
    }

    private String checksum(StringBuilder line) {

        StringBuilder str = new StringBuilder();
        byte b = 0;

        for (int i = 0; i < line.length() - 1; i += 2) {
            b += Integer.parseInt(line.substring(i, i + 2), 16);
        }

        b = (byte) (256 - b);

        return String.format("%02X", b);
    }




}



