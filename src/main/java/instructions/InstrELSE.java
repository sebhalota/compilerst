package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.TypeMismatchInParaError;
import instructions.assembler.JMP;
import instructions.assembler.Jump;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.Type;
import variables.VariableStack;

public class InstrELSE extends Instruction {

    public InstrELSE(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "InstrELSE";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        JMP jmp = new JMP(Jump.Type.IF_SUCCESS);
        assemblerInstructionList.add(jmp);

        NOP nop = new NOP(Label.Type.IF_CHOICE, getNextLabelName());
        assemblerInstructionList.add(nop);

    }

}
