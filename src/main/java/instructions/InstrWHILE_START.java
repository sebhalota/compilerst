package instructions;

import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.RequireBOOLexpError;
import instructions.assembler.Label;
import instructions.assembler.NOP;
import variables.Type;
import variables.VariableStack;

public class InstrWHILE_START extends Instruction {

    public InstrWHILE_START(CaretPosition caretPosition) {
        this.caretPosition = caretPosition;
    }

    @Override
    public CompilationError check(VariableStack variableStack) {
        return null;
    }

    @Override
    public String getName() {
        return "Instr_WHILE_START";
    }

    @Override
    public void generateCode(VariableStack variableStack) {

        NOP nop = new NOP(Label.Type.WHILE_START,getNextLabelName());
        assemblerInstructionList.add(nop);

    }

}
