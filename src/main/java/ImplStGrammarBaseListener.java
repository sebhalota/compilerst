import CaretPosition.CaretPosition;
import instructions.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.Interval;
import variables.*;


public class ImplStGrammarBaseListener extends StGrammarBaseListener {

    private StGrammarParser parser;
    private SymbolTable symbolTable;
    private InstructionList instructionList;

    public ImplStGrammarBaseListener(StGrammarParser parser, SymbolTable symbolTable, InstructionList instructionList) {
        this.parser = parser;
        this.symbolTable = symbolTable;
        this.instructionList = instructionList;
    }

    @Override
    public void exitEndOfDeclaration(StGrammarParser.EndOfDeclarationContext ctx) {
        instructionList.add(new InstrEndDeclarations(createCaretPosition(ctx)));
    }

    @Override
    public void exitEndOfProgram(StGrammarParser.EndOfProgramContext ctx) {
        instructionList.add(new InstrEndProgram(createCaretPosition(ctx)));
    }

    @Override
    public void enterDeclarationBOOL(StGrammarParser.DeclarationBOOLContext ctx) {
        StGrammarParser.DeclarationContext parentCtx = (StGrammarParser.DeclarationContext) ctx.getParent();

        boolean initialValue = false;
        if (ctx.TRUE() != null) initialValue = true;

        Symbol symbol = new SymbolBOOL(parentCtx.ID().toString(), initialValue, createCaretPosition(ctx));

        symbolTable.add(symbol);
        instructionList.add(new InstrDeclaration(symbol));

    }

    @Override
    public void enterDeclarationOUTPUT(StGrammarParser.DeclarationOUTPUTContext ctx) {
        StGrammarParser.DeclarationContext parentCtx = (StGrammarParser.DeclarationContext) ctx.getParent();

        boolean initialValue = false;
        if (ctx.TRUE() != null) initialValue = true;

        int port = Integer.parseInt(ctx.NUMBER(0).toString());
        int pin = Integer.parseInt(ctx.NUMBER(1).toString());

        Symbol symbol = new SymbolOUTPUT(parentCtx.ID().toString(), initialValue, port, pin, createCaretPosition(ctx));

        symbolTable.add(symbol);
        instructionList.add(new InstrDeclaration(symbol));

    }

    @Override
    public void enterDeclarationINPUT(StGrammarParser.DeclarationINPUTContext ctx) {
        StGrammarParser.DeclarationContext parentCtx = (StGrammarParser.DeclarationContext) ctx.getParent();

        boolean initialValue = false;
        if (ctx.TRUE() != null) initialValue = true;

        int port = Integer.parseInt(ctx.NUMBER(0).toString());
        int pin = Integer.parseInt(ctx.NUMBER(1).toString());

        Symbol symbol = new SymbolINPUT(parentCtx.ID().toString(), initialValue, port, pin, createCaretPosition(ctx));

        symbolTable.add(symbol);
        instructionList.add(new InstrDeclaration(symbol));
    }

    @Override
    public void enterDeclarationBYTE(StGrammarParser.DeclarationBYTEContext ctx) {
        StGrammarParser.DeclarationContext parentCtx = (StGrammarParser.DeclarationContext) ctx.getParent();

        int initialValue = 0;
        if (ctx.NUMBER() != null) initialValue = Integer.parseInt(ctx.NUMBER().toString());

        Symbol symbol = new SymbolBYTE(parentCtx.ID().toString(), initialValue, createCaretPosition(parentCtx));

        symbolTable.add(symbol);
        instructionList.add(new InstrDeclaration(symbol));

    }

    @Override
    public void enterDeclarationWORD(StGrammarParser.DeclarationWORDContext ctx) {
        StGrammarParser.DeclarationContext parentCtx = (StGrammarParser.DeclarationContext) ctx.getParent();

        int initialValue = 0;
        if (ctx.NUMBER() != null) initialValue = Integer.parseInt(ctx.NUMBER().toString());

        Symbol symbol = new SymbolWORD(parentCtx.ID().toString(), initialValue, createCaretPosition(parentCtx));

        symbolTable.add(symbol);
        instructionList.add(new InstrDeclaration(symbol));
    }

    @Override
    public void exitExpLT(StGrammarParser.ExpLTContext ctx) {
        instructionList.add(new InstrLT(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpNUMBER(StGrammarParser.ExpNUMBERContext ctx) {

        String value = ctx.NUMBER().toString();

        if (SymbolBYTE.isBYTE(value))
            instructionList.add(new InstrStoreBYTE(Integer.parseInt(value), createCaretPosition(ctx)));
        else if (SymbolWORD.isWORD(value))
            instructionList.add(new InstrStoreWORD(Integer.parseInt(value), createCaretPosition(ctx)));
        else
            System.out.println("ERROR");
    }

    @Override
    public void exitExpOR(StGrammarParser.ExpORContext ctx) {
        instructionList.add(new InstrOR(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpADD_SUB(StGrammarParser.ExpADD_SUBContext ctx) {
        if (ctx.ADD() != null) instructionList.add(new InstrADD(createCaretPosition(ctx)));
        if (ctx.SUB() != null) instructionList.add(new InstrSUB(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpEQ(StGrammarParser.ExpEQContext ctx) {
        instructionList.add(new InstrEQ(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpGT(StGrammarParser.ExpGTContext ctx) {
        instructionList.add(new InstrGT(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpNEQ(StGrammarParser.ExpNEQContext ctx) {
        instructionList.add(new InstrNEQ(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpLE(StGrammarParser.ExpLEContext ctx) {
        instructionList.add(new InstrLE(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpAND(StGrammarParser.ExpANDContext ctx) {
        instructionList.add(new InstrAND(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpID(StGrammarParser.ExpIDContext ctx) {
        instructionList.add(new InstrStoreVariable(ctx.ID().toString(), createCaretPosition(ctx)));
    }

    @Override
    public void exitExpGE(StGrammarParser.ExpGEContext ctx) {
        instructionList.add(new InstrGE(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpTRUE(StGrammarParser.ExpTRUEContext ctx) {
        instructionList.add(new InstrStoreBOOL(true, createCaretPosition(ctx)));
    }

    @Override
    public void exitExpFALSE(StGrammarParser.ExpFALSEContext ctx) {
        instructionList.add(new InstrStoreBOOL(false, createCaretPosition(ctx)));
    }

    @Override
    public void exitCommandASSIGN(StGrammarParser.CommandASSIGNContext ctx) {
        instructionList.add(new InstrASSIGN(ctx.ID().toString(), createCaretPosition(ctx)));
    }

    @Override
    public void exitExpIf(StGrammarParser.ExpIfContext ctx) {
        instructionList.add(new InstrIF(createCaretPosition(ctx)));
    }

    @Override
    public void exitCommandIF(StGrammarParser.CommandIFContext ctx) {
        instructionList.add(new InstrEND_IF(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpELSIF(StGrammarParser.ExpELSIFContext ctx) {
        instructionList.add(new InstrELSIF(createCaretPosition(ctx)));
    }

    @Override
    public void enterCommandELSE(StGrammarParser.CommandELSEContext ctx) {
        instructionList.add(new InstrELSE(createCaretPosition(ctx)));
    }

    @Override
    public void enterCommandWHILE(StGrammarParser.CommandWHILEContext ctx) {
        instructionList.add(new InstrWHILE_START(createCaretPosition(ctx)));
    }

    @Override
    public void exitExpWhile(StGrammarParser.ExpWhileContext ctx) {
        instructionList.add(new InstrWHILE(createCaretPosition(ctx)));
    }

    @Override
    public void exitCommandWHILE(StGrammarParser.CommandWHILEContext ctx) {
        instructionList.add(new InstrEND_WHILE(createCaretPosition(ctx)));
    }

    private CaretPosition createCaretPosition(ParserRuleContext ctx) {
        return new CaretPosition(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
    }

}
