import CaretPosition.CaretPosition;
import errors.CompilationError;
import errors.CompilationErrorList;
import errors.ErroneousInitialValueError;
import errors.ErrorListener;
import instructions.InstructionList;
import instructions.assembler.OpcodeGenerator;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import variables.*;

import java.io.*;

import java.util.Date;

public class Main {

    public static void main(String[] args) {

        long millisStartTime = System.currentTimeMillis();

        Messages messages = new Messages();
        Console console = new Console(args, messages);

//        CompilationError aaa = new CompilationError(new CaretPosition(),"as");
//        System.out.println(aaa.getMessage());

        OpcodeGenerator opcodeGenerator = new OpcodeGenerator("1010AB10AB10AB101010AB10AB10AB10", 0, 0);

        if (console.fileIsOk()) {

            CompilationErrorList compilationErrorsList = new CompilationErrorList();
            SymbolTable symbolTable = new SymbolTable(compilationErrorsList);
            InstructionList instructionList = new InstructionList(compilationErrorsList, symbolTable);

            StGrammarLexer lexer = new StGrammarLexer(console.getProgramCode());

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            StGrammarParser parser = new StGrammarParser(tokens);

            parser.removeErrorListeners();
            lexer.removeErrorListeners();
            ErrorListener errorListener = new ErrorListener(compilationErrorsList);
            parser.addErrorListener(errorListener);
            lexer.addErrorListener(errorListener);

            ParseTree tree = parser.program();

            ParseTreeWalker walker = new ParseTreeWalker();
            ImplStGrammarBaseListener listener = new ImplStGrammarBaseListener(parser, symbolTable, instructionList);
            walker.walk(listener, tree);

            Date date = new Date();
            if (compilationErrorsList.noErrors()) {
                console.generateHeader("COMPILATION SUCCESS", Long.toString(System.currentTimeMillis() - millisStartTime), date.toString(), Integer.toString(instructionList.getCodeSize()));
                instructionList.makeBackpatching();
                console.createAsmFile(instructionList.generateAssemblerCode());
                console.createHexFile(instructionList.generateHex());
            } else {
                console.generateHeader("COMPILATION FAILURE", Long.toString(System.currentTimeMillis() - millisStartTime), date.toString(), Integer.toString(instructionList.getCodeSize()));
                messages.add(Messages.Type.ERROR, compilationErrorsList.getErrorsMessages());
            }

        }

        System.out.println(messages.getMessages());

    }

}
