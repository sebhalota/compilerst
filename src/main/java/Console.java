import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Console {

    private static final String NO_PATH = "Enter the path to the file";
    private static final String NO_FILE = "File does not exist";
    private static final String HEX_FILE_ERROR = "Cannot create hex file";
    private static final String ASM_FILE_ERROR = "Compiler cannot create asm file";

    private StringBuffer programCode = new StringBuffer();
    private String pathAsm = "";
    private String pathHex = "";
    private boolean fileOk = false;
    private Messages messages;

    public Console(String[] args, Messages messages) {
        this.messages = messages;
        if (args.length == 0) {
            messages.add(Messages.Type.ERROR, NO_PATH);
        } else {
            readFile(args[0]);
        }
    }

    private void readFile(String filePath) {

        try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath))) {
            String path = deleteFilenameExtension(filePath);
            pathAsm = path + ".asm";
            pathHex = path + ".hex";
            readCode(fileReader);

            messages.add(Messages.Type.INFO, "Compile " + filePath);

        } catch (IOException e) {
            messages.add(Messages.Type.ERROR, NO_FILE);
        }

    }

    private void readCode(BufferedReader fileReader) {

        try {
            String line;
            while (true) {
                line = fileReader.readLine();
                if (line != null)
                    programCode.append(line);
                else break;
                fileOk = true;
            }

        } catch (IOException e) {
            messages.add(Messages.Type.ERROR, NO_FILE);
        }
    }

    private String deleteFilenameExtension(String filePath) {
        return filePath.substring(0, filePath.lastIndexOf('.'));
    }

    public boolean fileIsOk() {
        return fileOk;
    }

    public CharStream getProgramCode() {
        return CharStreams.fromString(programCode.toString());
    }

    public String getPathAsm() {
        return this.pathAsm;
    }

    public String getPathHex() {
        return this.pathHex;
    }

    public void createHexFile(String hex) {
        try (FileWriter fileWriter = new FileWriter(this.getPathHex())) {
            fileWriter.write(hex);
        } catch (IOException e) {
            messages.add(Messages.Type.ERROR, HEX_FILE_ERROR);
        }
    }

    public void createAsmFile(String asm) {
        try (FileWriter fileWriter = new FileWriter(this.getPathAsm())) {
            fileWriter.write(asm);
        } catch (IOException e) {
            messages.add(Messages.Type.ERROR, ASM_FILE_ERROR);
        }
    }

    public void generateHeader(String header, String timeMs, String date, String memorySizeBytes) {
        messages.add(Messages.Type.INFO, Messages.createDashes());
        messages.add(Messages.Type.INFO, header);
        messages.add(Messages.Type.INFO, Messages.createDashes());
        messages.add(Messages.Type.INFO,"Total time: " + timeMs+ " ms");
        messages.add(Messages.Type.INFO,"Finished at: " + date);
        messages.add(Messages.Type.INFO,"Memory use summary: " + memorySizeBytes + " bytes");

    }

}
