package variables;

import errors.CompilationErrorList;
import errors.ErroneousInitialValueError;
import errors.InvalidAddressError;
import errors.SeveralDeclarationError;

import java.util.ArrayList;
import java.util.List;

public class SymbolTable {
    private List<Symbol> list = new ArrayList<>();
    private CompilationErrorList compilationErrorList;
    private int freeRamAddress = 0x0060;

    public SymbolTable(CompilationErrorList compilationErrorList) {
        this.compilationErrorList = compilationErrorList;
    }

    public void add(Symbol symbol) {
        if (nameExist(symbol.getName())) {
            compilationErrorList.add(new SeveralDeclarationError(symbol.getCaretPosition(), symbol.getName()));
        } else if (!symbol.rangeIsOk())
            compilationErrorList.add(new ErroneousInitialValueError(symbol.getCaretPosition(), symbol.getName()));
        else {
            if (symbol instanceof RamAddress) {
                ((RamAddress) symbol).setAddress(freeRamAddress);
                freeRamAddress += symbol.size();
                list.add(symbol);
            } else if (symbol instanceof IOAddress) {
                if (!((IOAddress) symbol).portIsOk() || !((IOAddress) symbol).pinIsOk()) {
                    compilationErrorList.add(new InvalidAddressError(symbol.getCaretPosition(), ((IOAddress) symbol).getAddressAsString()));
                } else {
                    list.add(symbol);
                }
            }
        }
    }

    public Symbol find(String name) {
        for (Symbol s : list) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        return null;
    }

    private boolean nameExist(String name) {
        for (Symbol s : list) {
            if (s.getName().equals(name))
                return true;
        }
        return false;
    }

}
