package variables;

public enum Type {
    BOOL,
    BYTE,
    WORD;
}
