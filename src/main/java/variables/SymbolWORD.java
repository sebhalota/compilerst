package variables;

import CaretPosition.CaretPosition;
import instructions.assembler.AssemblerInstruction;
import instructions.assembler.LDI;
import instructions.assembler.LDS;
import instructions.assembler.STS;

import java.util.ArrayList;
import java.util.List;

public class SymbolWORD extends Symbol implements RamAddress{

    private int initialValue;
    private int address;

    public SymbolWORD(String name, int initialValue, CaretPosition caretPosition) {
        super(name, Type.WORD, caretPosition);
        this.initialValue = initialValue;
    }

    public SymbolWORD(String name, CaretPosition caretPosition) {
        super(name, Type.WORD, caretPosition);
        this.initialValue = 0;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getAddress() {
        return this.address;
    }

    @Override
    public boolean rangeIsOk() {
        if (initialValue >= 0 && initialValue <= 65535)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return 2;
    }

    @Override
    public String getInitialValueAsString() {
        return Integer.toString(initialValue);
    }

    public static boolean isWORD(String value) {
        if (value != null) {
            if (isNumeric(value)) {
                if (isWordRange(Integer.parseInt(value))) return true;
            }
        }
        return false;
    }

    private static boolean isWordRange(int value) {
        if (value >= 0 && value <= 65535)
            return true;
        else
            return false;
    }

    public List<AssemblerInstruction> declarationCode() {
        List<AssemblerInstruction> list = new ArrayList<>();

        list.addAll(storeLiteralCode((byte)16,initialValue));
        list.addAll(assignCode((byte) 16));

        return list;
    }

    public List<AssemblerInstruction> assignCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            STS sts = new STS((byte)(registerR+i), (byte) (getAddress()+i));
            list.add(sts);
        }

        return list;
    }

    public List<AssemblerInstruction> storeLiteralCode(byte registerR, int value) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            LDI ldi = new LDI((byte) ((value & (0xFF << (8 * i))) >>> (8 * i)), (byte) (registerR + i));
            list.add(ldi);
        }

        return list;
    }

    public List<AssemblerInstruction> storeVariableCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            LDS lds = new LDS((byte) (registerR + i), (short) (getAddress() + i));
            list.add(lds);
        }

        return list;
    }


}
