package variables;

import java.util.Stack;


public class VariableStack {

    private Stack<Structure> stack = new Stack();
    private static byte registerR = 16;

    public class Structure {
        private Symbol symbol;
        private byte registerR;

        private Structure(Symbol symbol, byte registerR) {
            this.symbol = symbol;
            this.registerR = registerR;
        }

        public Symbol getSymbol() {
            return this.symbol;
        }

        public byte getRegisterR() {
            return this.registerR;
        }

        private void setRegisterR(byte registerR) {
            this.registerR = registerR;
        }

    }

    public void push(Symbol symbol) {
        Structure s = new Structure(symbol, registerR);
        for (int i = 0; i < symbol.size(); i++) registerR++;
        stack.push(s);
    }

    public void changeLastVariableType(Type type) {
        Structure s = this.pop();
        Symbol symbol = Symbol.changeType(s.getSymbol(),type);
        this.push(symbol);
    }

    public void changePenultVariableType(Type type) {
        Structure s1 = this.pop();
        Structure s2 = this.pop();

        Symbol symbol = Symbol.changeType(s2.getSymbol(),type);
        this.push(symbol);
        this.push(s1.getSymbol());
    }

    public Structure pop() {
        Structure s = null;
        if(!stack.empty()) {
            s = stack.pop();
            registerR = (byte) (registerR - s.getSymbol().size());
        }
        return s;
    }

    public Structure[] peek(int quantity) {
        Structure[] s = new Structure[quantity];
        int stackIndex = stack.size() - 1;

        for (int i = 0; i < quantity; i++) {
            s[i] = stack.get(stackIndex - i);
        }

        return s;
    }

    public Structure peekOne() {
        Structure[] arr = peek(1);
        return arr[0];
    }

    public boolean isSizeTry(int[] array) {
        if(array == null) return false;
        else return array.length==3;
    }

    public boolean even(int x) {
        return x%2==0;
    }


}
