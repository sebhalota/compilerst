package variables;

public interface RamAddress {
    void setAddress(int address);
    int getAddress();

}
