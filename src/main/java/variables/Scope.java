package variables;

public enum Scope {
    GLOBAL,
    IN,
    OUT,
    IN_OUT,
    INTERNAL;
}
