package variables;

import CaretPosition.CaretPosition;
import instructions.assembler.AssemblerInstruction;
import instructions.assembler.LDI;
import instructions.assembler.LDS;
import instructions.assembler.STS;

import java.util.ArrayList;
import java.util.List;

public class SymbolBOOL extends Symbol implements RamAddress {

    private boolean initialValue;
    private int address;

    public SymbolBOOL(String name, boolean initialValue, CaretPosition caretPosition) {
        super(name, Type.BOOL, caretPosition);
        this.initialValue = initialValue;
    }

    public SymbolBOOL(String name, CaretPosition caretPosition) {
        super(name, Type.BOOL, caretPosition);
        this.initialValue = false;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getAddress() {
        return this.address;
    }

    public boolean rangeIsOk() {
        return true;
    }

    public int size() {
        return 1;
    }

    public String getInitialValueAsString() {
        return Boolean.toString(initialValue);
    }

    public List<AssemblerInstruction> declarationCode() {
        List<AssemblerInstruction> list = new ArrayList<>();

        if (initialValue) {
            list.addAll(storeLiteralCode((byte) 16, (byte) 1));
        } else {
            list.addAll(storeLiteralCode((byte) 16, (byte) 0));
        }

        list.addAll(assignCode((byte)16));

        return list;
    }

    public List<AssemblerInstruction> assignCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            STS sts = new STS((byte) (registerR + i), (byte) (getAddress() + i));
            list.add(sts);
        }

        return list;
    }

    public List<AssemblerInstruction> storeLiteralCode(byte registerR, int value) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            LDI ldi = new LDI((byte) ((value & (0xFF << (8 * i))) >>> (8 * i)), (byte) (registerR + i));
            list.add(ldi);
        }

        return list;
    }

    public List<AssemblerInstruction> storeVariableCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            LDS lds = new LDS((byte) (registerR + i), (short) (getAddress() + i));
            list.add(lds);
        }

        return list;
    }


}
