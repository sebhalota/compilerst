package variables;

import CaretPosition.CaretPosition;
import instructions.assembler.AssemblerInstruction;

import java.util.List;

public abstract class Symbol {
    private String name;
    private Type type;
    private CaretPosition caretPosition;

    protected Symbol(String name, Type type, CaretPosition caretPosition) {
        this.name = name;
        this.type = type;

        try {
            this.caretPosition = (CaretPosition) caretPosition.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

    protected static boolean isNumeric(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String getInfo() {
        return name + type + getInitialValueAsString() + caretPosition.getLine() + caretPosition.getCharPositionInLine();
    }

    public String getName() {
        return this.name;
    }

    public Type getType() {
        return this.type;
    }

    public CaretPosition getCaretPosition() {

        CaretPosition caretPosition = new CaretPosition();

        try {
            caretPosition = (CaretPosition) this.caretPosition.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return caretPosition;

    }

    public abstract boolean rangeIsOk();

    public abstract int size();

    public abstract String getInitialValueAsString();

    public abstract List<AssemblerInstruction> declarationCode();

    public abstract List<AssemblerInstruction> assignCode(byte registerR);

    public abstract List<AssemblerInstruction> storeLiteralCode(byte registerR, int value);

    public abstract List<AssemblerInstruction> storeVariableCode(byte registerR);

    public static Symbol changeType(Symbol s, Type type) {
        Symbol symbol = null;

        if (type == Type.BOOL) {
            symbol = new SymbolBOOL(s.getName(), s.getCaretPosition());
        } else if (type == Type.BYTE) {
            symbol = new SymbolBYTE(s.getName(), s.getCaretPosition());
        } else if (type == Type.WORD) {
            symbol = new SymbolWORD(s.getName(), s.getCaretPosition());
        }

        return symbol;
    }

}
