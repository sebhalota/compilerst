package variables;

public interface IOAddress {

    byte[][] registers = {
            {0x19,0x16,0x13,0x10},
            {0x1A,0x17,0x14,0x11},
            {0x1B,0x18,0x15,0x12}
    };

    enum Port {
        PIN(0),
        DDR(1),
        PORT(2);

        int index;

        Port(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }


    }


//        portMap.put((byte)0,(byte)0x1B);
//        portMap.put((byte)1,(byte)0x18);
//        portMap.put((byte)2,(byte)0x15);
//        portMap.put((byte)3,(byte)0x12);
//
//        ddrMap.put((byte)0,(byte)0x1A);
//        ddrMap.put((byte)1,(byte)0x17);
//        ddrMap.put((byte)2,(byte)0x14);
//        ddrMap.put((byte)3,(byte)0x11);
//
//        pinMap.put((byte)0,(byte)0x19);
//        pinMap.put((byte)1,(byte)0x16);
//        pinMap.put((byte)2,(byte)0x13);
//        pinMap.put((byte)3,(byte)0x10);

    void setPort(int port);
    int getPort();
    boolean portIsOk();

    void setPin(int pin);
    int getPin();
    boolean pinIsOk();

    String getAddressAsString();
}
