package variables;

import CaretPosition.CaretPosition;
import instructions.assembler.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SymbolOUTPUT extends Symbol implements IOAddress {

    private boolean initialValue;
    private int port;
    private int pin;

    public SymbolOUTPUT(String name, boolean initialValue, int port, int pin, CaretPosition caretPosition) {
        super(name, Type.BOOL, caretPosition);
        this.initialValue = initialValue;
        this.port = port;
        this.pin = pin;
//
//        portMap.put((byte)0,(byte)0x1B);
//        portMap.put((byte)1,(byte)0x18);
//        portMap.put((byte)2,(byte)0x15);
//        portMap.put((byte)3,(byte)0x12);
//
//        ddrMap.put((byte)0,(byte)0x1A);
//        ddrMap.put((byte)1,(byte)0x17);
//        ddrMap.put((byte)2,(byte)0x14);
//        ddrMap.put((byte)3,(byte)0x11);
//
//        pinMap.put((byte)0,(byte)0x19);
//        pinMap.put((byte)1,(byte)0x16);
//        pinMap.put((byte)2,(byte)0x13);
//        pinMap.put((byte)3,(byte)0x10);

    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return this.port;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getPin() {
        return this.pin;
    }

    public String getAddressAsString() {
        return "%QX" + port + "." + pin;
    }

    public boolean portIsOk() {
        if(port >= 0 && port <=3) return true;
        else return false;
    }

    public boolean pinIsOk() {
        if(pin >= 0 && pin <=7) return true;
        else return false;
    }

    public boolean rangeIsOk() {
        return true;
    }

    public int size() {
        return 1;
    }

    public String getInitialValueAsString() {
        return Boolean.toString(initialValue);
    }

    public List<AssemblerInstruction> declarationCode() {
        List<AssemblerInstruction> list = new ArrayList<>();

        SBI sbi = new SBI(IOAddress.registers[Port.DDR.getIndex()][port],(byte)pin);

        list.add(sbi);

        if (initialValue) {
            list.addAll(storeLiteralCode((byte) 16, (byte) 1));
        } else {
            list.addAll(storeLiteralCode((byte) 16, (byte) 0));
        }

        list.addAll(assignCode((byte)16));

        return list;
    }

    public List<AssemblerInstruction> assignCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();

        SBRS sbrs = new SBRS(registerR,(byte)0);
        list.add(sbrs);

        CBI cbi = new CBI(IOAddress.registers[Port.PORT.getIndex()][port],(byte)pin);
        list.add(cbi);

        SBRC sbrc = new SBRC(registerR,(byte)0);
        list.add(sbrc);

        SBI sbi = new SBI(IOAddress.registers[Port.PORT.getIndex()][port],(byte)pin);
        list.add(sbi);

        NOP nop = new NOP();
        list.add(nop);

        return list;
    }

    public List<AssemblerInstruction> storeLiteralCode(byte registerR, int value) {
        List<AssemblerInstruction> list = new ArrayList<>();

        for (int i = 0; i < size(); i++) {
            LDI ldi = new LDI((byte) ((value & (0xFF << (8 * i))) >>> (8 * i)), (byte) (registerR + i));
            list.add(ldi);
        }

        return list;
    }

    public List<AssemblerInstruction> storeVariableCode(byte registerR) {
        List<AssemblerInstruction> list = new ArrayList<>();


        SBIS sbis = new SBIS(IOAddress.registers[Port.PIN.getIndex()][port],(byte)pin);
        list.add(sbis);

        LDI ldi1 = new LDI((byte)0,registerR);
        list.add(ldi1);

        SBIC sbic = new SBIC(IOAddress.registers[Port.PIN.getIndex()][port],(byte)pin);
        list.add(sbic);

        LDI ldi2 = new LDI((byte)1,registerR);
        list.add(ldi2);

        return list;
    }

}
