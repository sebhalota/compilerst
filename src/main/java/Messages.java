import java.util.ArrayList;
import java.util.List;

public class Messages {

    enum Type {
        INFO("[INFO]"),
        ERROR("[ERROR]");

        private String name;

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private class Structure {
        private Type type;
        private String message;

        public Structure(Type type, String message) {
            this.type = type;
            this.message = message;
        }

        @Override
        public String toString() {
            return type.getName() + " " + message + "\n\r";

        }
    }

    private List<Structure> list = new ArrayList<>();

    public void add(Type type, String msg) {
        list.add(new Structure(type, msg));
    }

    public void add(Type type, List<String> msgs) {
        for(String m:msgs) {
            list.add(new Structure(type,m));
        }
    }

    public String getMessages() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Structure s : list) {
            stringBuilder.append(s.toString());
        }

        return stringBuilder.toString();
    }

    public static String createDashes() {
        return "------------------------------------------------------------------------";
    }

}
