package errors;

import CaretPosition.CaretPosition;

public class TypeMismatchError extends CompilationError {

    public TypeMismatchError(CaretPosition caretPosition, String type1, String type2) {
        super(caretPosition, getMessageText(type1, type2));
    }

    private static String getMessageText(String type1, String type2) {
        if (type1 == null) {
            throw new IllegalArgumentException("type1 cannot be null");
        } else if (type2 == null) {
            throw new IllegalArgumentException("type2 cannot be null");
        } else {
            return "Type mismatch. Cannot convert '" + type1 + "' to '" + type2 + "'";
        }
    }
}
