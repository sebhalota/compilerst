package errors;

import CaretPosition.CaretPosition;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

public class ErrorListener extends BaseErrorListener {

    private CompilationErrorList compilationErrorList;

    public ErrorListener(CompilationErrorList compilationErrorList) {
        if (compilationErrorList == null) {
            throw new IllegalArgumentException("compilationErrorList cannot be null");
        } else {
            this.compilationErrorList = compilationErrorList;
        }
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line, int charPositionInLine,
                            String msg,
                            RecognitionException e) {

        compilationErrorList.add(new CompilationError(new CaretPosition(line, charPositionInLine), msg));

    }

}
