package errors;

import CaretPosition.CaretPosition;

public class IdNotDefinedError extends CompilationError {

    public IdNotDefinedError(CaretPosition caretPosition, String variableName) {
        super(caretPosition, getMessageText(variableName));
    }

    private static String getMessageText(String variableName) {
        if (variableName == null) {
            throw new IllegalArgumentException("variableName cannot be null");
        } else {
            return "Identifer '" + variableName + "' not defined";
        }
    }

}
