package errors;

import CaretPosition.CaretPosition;

public class SeveralDeclarationError extends CompilationError {

    public SeveralDeclarationError(CaretPosition caretPosition, String variableName) {
        super(caretPosition, getMessageText(variableName));
    }

    private static String getMessageText(String variableName) {
        if (variableName == null) {
            throw new IllegalArgumentException("variableName cannot be null");
        } else {
            return "Several declarations with the same identifier '" + variableName + "'";
        }
    }


}
