package errors;

import CaretPosition.CaretPosition;

public class RequireBOOLexpError extends CompilationError {

    public RequireBOOLexpError(CaretPosition caretPosition, String name) {
        super(caretPosition, getMessageText(name));
    }

    private static String getMessageText(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        } else {
            return "'" + name + "' requires 'BOOL' expression as condition";
        }
    }


}
