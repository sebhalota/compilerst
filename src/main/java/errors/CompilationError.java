package errors;

import CaretPosition.CaretPosition;

public class CompilationError {

    private CaretPosition caretPosition;
    private String message;

    public CompilationError(CaretPosition caretPosition, String message) {
        if(caretPosition == null) throw new IllegalArgumentException("CaretPosition cannot be null.");
        if(message == null) throw new IllegalArgumentException("Message cannot be null.");

        try {
            this.caretPosition = (CaretPosition) caretPosition.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        this.message = message;

    }

    public String getMessage() {
        return "[" + caretPosition.getLine() + "," + caretPosition.getCharPositionInLine() + "] " + message;
    }

}
