package errors;

import CaretPosition.CaretPosition;

public class InvalidAddressError extends CompilationError {

    public InvalidAddressError(CaretPosition caretPosition, String address) {
        super(caretPosition, getMessageText(address));
    }

    private static String getMessageText(String address) {
        if (address == null) {
            throw new IllegalArgumentException("address cannot be null");
        } else {
            return "Invalid address " + address;
        }
    }

}
