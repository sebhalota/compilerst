package errors;

import java.util.ArrayList;
import java.util.List;

public class CompilationErrorList  {

    private List<CompilationError> list = new ArrayList<>();

    public void add(CompilationError compilationError) {
        if(compilationError == null) {
            throw new IllegalArgumentException("compilationError cannot be null");
        }
        else {
            list.add(compilationError);
        }
    }

    public boolean noErrors() {
        return list.size()==0;
    }

    public List<String> getErrorsMessages() {
        List<String> msgs = new ArrayList<>(list.size());

        for(CompilationError e : list) {
            msgs.add(e.getMessage());
        }

        return msgs;
    }


}
