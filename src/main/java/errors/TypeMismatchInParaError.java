package errors;

import CaretPosition.CaretPosition;

public class TypeMismatchInParaError extends CompilationError {

    public TypeMismatchInParaError(CaretPosition caretPosition, int paraNumber, String paraSign, String type) {
        super(caretPosition, getMessageText(paraNumber, paraSign, type));
    }

    private static String getMessageText(int paraNumber, String paraSign, String type) {
        if (paraSign == null) {
            throw new IllegalArgumentException("paraSign cannot be null");
        } else if (type == null) {
            throw new IllegalArgumentException("type cannot be null");
        } else {
            return "Type mismatch in parameter " + paraNumber + " of " + paraSign + ". Cannot convert " + type;
        }

    }
}
